// Routes define when a particular controller will be used

const express = require("express");

// Create a Router instance that functions as a routing system

const router = express.Router();


// Import the taskControllers

const taskController = require("../controllers/taskControllers");


// Route to get all the tasks
// endpoint: localhost:3001/tasks

router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})


// Route to create a task

router.post("/", (req, res) => {

	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})


// Route to delete a task

// endpoint: localhost:3001/tasks/123456

router.delete("/:id", (req, res) => {

	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


// Route to update a task

router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})




// s31 Activity

// 1. Create a route for getting a specific task.

router.get("/:id", (req, res) => {
	taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController));
})



// 4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.

// POSTMAN
// s31 Activity > getting a specific task


// 5. Create a route for changing the status of a task to "complete".

router.put("/:id/complete", (req, res) => {
	taskController.updateTaskStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


// 8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.

// POSTMAN
// s31 Activity > changing the status of a task


// Export the router object to be used in index.js

module.exports = router;


